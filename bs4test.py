import json
from pymysql.cursors import Cursor
import requests
import urllib.parse
import pymysql
import pandas as pd
from bs4 import BeautifulSoup
from controller import quickReply,covidInfo,tbsFunction
from highcharts import Highchart

df = pd.read_excel('G://paytypeRate.xlsx')

# chart = df.plot()
col = df.columns.ravel()
data1 = df['北四區'].tolist()
row = df.iloc[0]

print (row)
H=Highchart(width=850,height=400)
data = [
    {
        'name': "Microsoft Internet Explorer",
        'y':56.33
    },
    {
        'name': "Chrome",
        'y': 24.03,
        'sliced': True,
        'selected':True,
    },
    {
        'name' : "Firefox",
        'y': 10.38
    },
    {
        'name': "Safari",
        'y': 4.77
    },
    {
        'name' : "Opera",
        'y': 0.91
    },
    {
        'name' : "Proprietary or Undetectable",
        'y': 0.2
    }]
options ={
    'chart' : 
    {
        'plotBackgroundColor': None,#背景色
        'plotBorderwidth': None, # 边框宽度
        'plotShadow': False #外围的明影边框是否显示
    },
    'title':{
        'text':'浏览器市场额占比'
    },
    'tooltip':{
          'pointFormat': 'fseries.name}: <b>
        (point .percentage:.1f)%</b>'
    }

}
# -----------------------------


