from linebot.models import *
import json
import requests
from urllib.request import urlopen
from OpenSSL import SSL
from bs4 import BeautifulSoup


# 當日covid19資料>>回傳flex訊息
def getCovidInfo():
    # htmlUrl = urlopen("https://covid-19.nchc.org.tw/")
    # htmlSoup = BeautifulSoup(htmlUrl,"html.parser")

    # 解析網頁ssl認證
    requests.packages.urllib3.disable_warnings()

    htmlUrl = requests.get("https://covid-19.nchc.org.tw/",verify=False)
    htmlSoup = BeautifulSoup(htmlUrl.text,"html.parser")
    covidAry = []
    # 累計確診 
    confirmedData = htmlSoup.find_all("h1",class_= "country_confirmed mb-1 text-success")[0].get_text()
    # 新增確診
    todayConfirm = htmlSoup.find_all("h1",class_="country_recovered mb-1 text-info")[0].get_text()
    todayConfirmLoc = htmlSoup.find_all("span",class_="country_confirmed_percent")[1].get_text()
    # 累計死亡
    confirmedDead = htmlSoup.find_all("h1",class_="country_deaths mb-1 text-dark")[0].get_text()
    # 合併資料
    covidAry.append(confirmedData)
    covidAry.append(todayConfirm)
    covidAry.append(todayConfirmLoc)
    covidAry.append(confirmedDead)

    # LINE訊息 >> 丟入json陣列
    covidCard = json.load(open('jsonfile/covidCard.json','r',encoding='utf-8'))
    # # 累計確診
    # covidCard['body']['contents'][0]['text'] += covidAry[0]
    # # 當日確診
    # covidCard['body']['contents'][1]['text'] += covidAry[1]
    # # 累計死亡
    # covidCard['body']['contents'][2]['text'] += covidAry[2]

    # forloop版本
    for i in range(len(covidAry)):
        covidCard['body']['contents'][i]['text'] += covidAry[i]

    return covidCard
    # 測試用
    # return covidAry