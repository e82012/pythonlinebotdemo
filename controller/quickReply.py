from linebot.models import *

def quickReply_1():
    quick_message = TextSendMessage(
    text="文字訊息",
    quick_reply=QuickReply(
        items=[
            QuickReplyButton(
                action=PostbackAction(label="Postback",data="回傳資料")
                ),
            QuickReplyButton(
                action=MessageAction(label="文字訊息",text="回傳文字")
                ),
            QuickReplyButton(
                action=DatetimePickerAction(label="時間選擇",data="時間選擇",mode='datetime')
                ),
            QuickReplyButton(
                action=CameraAction(label="拍照")
                ),
            QuickReplyButton(
                action=CameraRollAction(label="相簿")
                ),
            QuickReplyButton(
                action=LocationAction(label="傳送位置")
                )
            ]
        )
    )
    return quick_message