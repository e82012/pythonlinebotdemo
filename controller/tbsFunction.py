import json
import sys
sys.path.append(".")
import config.dbconnect as db_c


# 取的門市資料
def getStoreInfo(storename):
    # DB連接
    db = db_c.db_connect()
    cursor = db.cursor()
    # 檢查storename查詢格式
    if(storename.endswith('門市')):
        storename = storename.split("門市",1)
        sql = "SELECT * FROM tbs_store WHERE storename = '"+storename[0]+"'"
        cursor.execute(sql)
        result = cursor.fetchall()
        storeInfo = [] 
        if(len(result)>0):
            # 店家資訊
            storeInfo.append([result[0][2]]) #店名
            storeInfo.append([result[0][16]]) #地址
            storeInfo.append([result[0][18]]) #電話
            # 資料帶入json
            storeInfoCard = json.load(open("jsonfile/storeInfo.json"))
            # jsonCard的資料位置
            # title
            storeInfoCard['body']['contents'][0]['text'] = storeInfo[0][0]
            # address
            storeInfoCard['body']['contents'][2]['contents'][0]['contents'][1]['text'] = storeInfo[1][0]
            # phone
            storeInfoCard['body']['contents'][2]['contents'][1]['contents'][1]['text'] = storeInfo[2][0]
            
        else:
            storeInfoCard="門市名稱錯誤"

        return storeInfoCard
