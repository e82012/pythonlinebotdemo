from flask import Flask, request, abort
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import *
import json,requests
from controller import quickReply,covidInfo,tbsFunction




app = Flask(__name__)
# LINE BOT info
line_bot_api = LineBotApi('MHcSPURSETquEBrT9BuH8cggRwx57f1qHgoCoCPfWvw1Gm8knuMZlawNQM4q7Jpv/IuiQY5Do4AcJilJt84jx6pW9ji10bOkgRcY+Aj63/kxYnXU0S+6tkiYK7R4TNOJhb1/Hd7WfD/5JB0PLB2/MgdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('dda13efa891dfa3afffdef447259b410')

@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    print(body)
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'
    

# Message event
@handler.add(MessageEvent)
def handle_message(event):
    # 訊息格式
    message_type = event.message.type
    # 使用者ID
    user_id = event.source.user_id
    # token
    reply_token = event.reply_token
    # 訊息文字
    message = event.message.text
    
    # 以下回應開始
    if(message == 'ID'):
        text_message = TextSendMessage(text = user_id)
        line_bot_api.reply_message(reply_token, text_message)
    if(message == 'confirm'):
        FlexMessage = json.load(open('jsonfile/card.json','r',encoding='utf-8'))
        line_bot_api.reply_message(reply_token, FlexSendMessage('confirm',FlexMessage)) 
    # 疫情卡片
    if(message == 'covid'):
        covid_ary = covidInfo.getCovidInfo()
        line_bot_api.reply_message(reply_token, FlexSendMessage('confirm',covid_ary))
    # 快速回覆測試
    if(message == 'select'):
        quickReplyMessage = quickReply.quickReply_1()
        line_bot_api.reply_message(reply_token,quickReplyMessage)
    # 查詢店家資訊
    if(message.endswith('門市')):
        storeInfo = tbsFunction.getStoreInfo(message)
        if(storeInfo == '門市名稱錯誤'):
            line_bot_api.reply_message(reply_token, TextSendMessage(text="門市名稱錯誤"))
        else:
            line_bot_api.reply_message(reply_token, FlexSendMessage('storeInfo',storeInfo))
    # 其他>>重複回應
    else:
        line_bot_api.reply_message(reply_token, TextSendMessage(text=message))
        


import os
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 8888))
    app.run(host='0.0.0.0', port=port)